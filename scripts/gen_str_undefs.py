#!/usr/bin/env python
"""Generate all the undefs required for the platform strings.

This will rebuild the strings.pnfo file under the lang subdirectory.

"""
import re
import sys

MARKER = "// BEGIN GENERATED CODE //"

if len(sys.argv) >= 1:
    FILENAME = sys.argv[1]
else:
    print "usage gen_str_undefs.py FILENAME"
    sys.exit(1)

fd = open(FILENAME, 'r')

lines_out = []

tags = dict()

for line in fd:
    if MARKER in line:
        break
    lines_out.append(line.strip())
    if line.strip() != "":
        parts = re.split('\s+', line)
        if len(parts) >= 9:
            if parts[0] == "-1" and parts[9] != "" and parts[9] not in tags:
                tags[parts[9]] = 1

fd.close()
fd = open(FILENAME, 'w')
fd.write("\n".join(lines_out))
fd.write("\n" + MARKER + "\n")
fd.write("\n".join(["#undef %s" % i for i in sorted(tags.keys())]))
